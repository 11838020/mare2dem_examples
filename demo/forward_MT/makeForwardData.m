
clear st;

sFile = 'demo_mt.emdata'; 
topo  = load('../Topo.txt');

st.stMT.frequencies  = logspace(-3,-1,11);
st.stMT.rx_y         = 0:2000:24000;
st.stMT.rx_type      = 'marine';  % 'marine' places receivers 0.1 m above topo.
st.stMT.lTE          = true;      % create both TE and TM mode data
st.stMT.lTM          = true;

% make data:
m2d_makeDataFile(sFile,topo,'forward',st)

% plot .resistivity file with MT receivers on top to confirm receivers are
% located at the correct positions and depth:
plotMARE2DEM('newest'); % 'newest' plots the most recent .resistivity file
 
 
