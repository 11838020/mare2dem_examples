
This directory has examples for forward and inverse modeling of MT and
CSEM data using MARE2DEM.

The file topo.txt contains the position and depth of the topography
used in the models.

Run MARE2DEM using for example using 8 mpi processes on a laptop:

mpirun -n 8 MARE2DEM demo.0.resistivity

When completed, MARE2DEM outputs the response file demo.0.resp, which
can be viewed in plotMARE2DEM_MT.m or plotMARE2DEM_CSEM.m, depending on
the particular data type.

For inversion parameter grids (i.e. models with free parameters) you can
use the same command:

mpirun -n 8 MARE2DEM  demo.0.resistivity

MARE2DEM will output three files for each inversion iteration
demo.<iteration#>.resistivity, demo.<iteration#>.resp and
demo.<iteration#>.sensitivity.
