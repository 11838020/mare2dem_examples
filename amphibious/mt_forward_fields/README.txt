This example shows how to use MARE2DEM to compute the raw MT fields in a
cross section of the 2D model.

makeForwardData.m  makes a data file with a grid of receivers
distributed across the amphibious 2D model.

After running MARE2DEM, plotFields.m can be used to plot the results as
a 2D section showing the TE and TM mode x directed fields, polarization
ellipses for the auxiliary y,z field vectors, and the Poynting vector
showing the time-averaged direction of energy flow.
