 
sFile = 'amphibious.emdata'; 

topo =  []; % don't need topo since fixed y and z positions used below

clear st 
st.stMT.frequencies  = [ 1/60]; % TE response cusps are around 60 s, so let's look at that period

% grid of receivers:

% large:
% y     = [-200000:1000:200000];
% z     = [-15000:1000:100000] +.1;

% medium:
% y     = [-20000:200:35000];
% z     = [-1500:200:20000] +.1;

%closeup:
%  y     = [8000:50:23000];
%  z     = [-500:50:4000] +.1;

% closeup2
 y     = [2000:25:8000];
 z     = [100:25:2500] +.1;
 
 
[Y,Z] = meshgrid(y,z);

st.stMT.rx_y         = Y(:);
st.stMT.rx_z         = Z(:);
st.stMT.lMTFields    = true;  

% make data:
m2d_makeDataFile(sFile,topo,'forward',st)

% plot .resistivity file with MT receivers on top to confirm receivers are
% located at the correct positions and depth:
plotMARE2DEM('newest'); % 'newest' plots the most recent .resistivity file
 
 