# README #


### Example files for MARE2DEM ###

* This repository holds examples files that are helpful for learning how to use MARE2DEM. 

### Current Examples ###
*  `demo/`  This is the main example setup for MARE2DEM and includes
folders and scripts for MT and CSEM forward modeling, synthetic data generation and
inversions for MT, CSEM and joint MT-CSEM data. Also includes examples showing 
how to use add penalty cut to line segments in the inversion grid, and how to use 
MARE2DEM to estimate static shifts for selected MT stations.

* `wannamaker_etal_1986_hill/` Example forward modeling of MT responses
over a trapezoidal hill from a classic MT paper on topography effects.

* `amphibious/` Example showing how to setup MT data for amphibious
profiles. Also includes examples showing how to compute the raw MT
electric fields in a cross section with scripts that make Poynting vector and
polariation ellipse plots on top of the 2D model structure.

* `mt fields/`  Examples showing how to compute raw MT fields along the
surface of a segmented slab model, and for making cross sections plots for conductive
and resistive prisms models

* `crosswell/` Crosswell EM example for magnetic transmitters and receivers.

 
### Requirements ###

* You can run the MATLAB scripts and view precomputed results included in
the repository using the MATLAB tools included in the mare2dem_matlab
repository.

* If you want to compute forward responses and run inversions on any of
the examples, you will need to install the MARE2DEM source code in
repository mare2dem_source
 
 
 