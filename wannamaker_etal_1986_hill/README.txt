This is an example showing how to forward model MT responses along
topography. This particular model was considered in a seminal paper on
topographic effects in MT responses:

Wannamaker, P.E., Stodt, J.A., Rijo, L., 1986. Two-dimensional
topographic responses in magnetotellurics modeled using finite elements.
Geophysics 51, 2131–2144.


makeForwardData.m  makes the data file for forward modeling with
MARE2DEM. It is configured to create land MT stations that have slope
parallel Ey and horizontal Hy, as typical for land MT installations.

After running MARE2DEM, the .resp file results can be viewed with
plotMARE2DEM_MT.m. Make sure to change the "Plot Type" menu to be
"profile plot" so that the MT responses at a given frequency are shown
as a function of receiver y position.

Check out how the MARE2DEM results compare with Figures 4-6 in
Wannamaker et al. (1986).

