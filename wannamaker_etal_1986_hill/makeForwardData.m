 
sFile = 'hill.emdata';   % output file name

topo = load('hill.txt');
st.stMT.frequencies  = [2 50 2000];
st.stMT.rx_y         = [-1500:20:1500];
st.stMT.rx_type      = 'land';  % 'land' places Rx 0.1 m below topo. Ey is parallel to slope. By is horizontal.
st.stMT.lTE          = true;    % create both TE and TM mode data
st.stMT.lTM          = true;
st.stMT.lTipper      = true;

% make data:
m2d_makeDataFile(sFile,topo,'forward',st)

% plot .resistivity file with MT receivers on top to confirm receivers are
% located at the correct positions and depth:
plotMARE2DEM('newest'); % 'newest' plots the most recent .resistivity file
 
