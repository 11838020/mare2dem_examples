 
fileName = 'cylinder.0.resp';

 
st = m2d_readEMData2DFile(fileName);

% peel off substructures:
stMT = st.stMT;
DATA = st.DATA;
 
% Get complex fields:
[Ex,iRx,iFq] = getComplexField(DATA,'ex');
[Ey,iRx,iFq] = getComplexField(DATA,'ey');
[Ez,iRx,iFq] = getComplexField(DATA,'ez');
[Hx,iRx,iFq] = getComplexField(DATA,'hx');
[Hy,iRx,iFq] = getComplexField(DATA,'hy');
[Hz,iRx,iFq] = getComplexField(DATA,'hz');

% Convert to arrays that are nRx x nFreq:
y = unique(stMT.receivers(:,2));
z = unique(stMT.receivers(:,3));
ny = length(y);
nz = length(z);
nf = length(stMT.frequencies);

Ex = reshape(Ex,nz,ny,nf);
Ey = reshape(Ey,nz,ny,nf);
Ez = reshape(Ez,nz,ny,nf);
Hx = reshape(Hx,nz,ny,nf);
Hy = reshape(Hy,nz,ny,nf);
Hz = reshape(Hz,nz,ny,nf);

% Poynting vector components:
Sy_TE = real(-Ex.*conj(Hz))/2;
Sz_TE = real( Ex.*conj(Hy))/2;
Sy_TM = real( Hx.*conj(Ez))/2;
Sz_TM = real(-Hx.*conj(Ey))/2;

% Make plots:
iskip = 3; % plot every 3rd grid point as ellipses and poynting vectors, otherwise too many
figure;

nf =  length(stMT.frequencies);

for i = 1:nf
    
    subplot(2,nf,nf*(i-1)+1);
    sub_pcolorField(y,z,Ex(:,:,i),'log_{10}|Ex| (V/m)');

    sub_plotPoynting(iskip,y,z,Sy_TE,Sz_TE,'k');
    sub_polarizationEllipse(iskip,y,z,Hy(:,:,1),Hz(:,:,1),'w',1) 
    m2d_plot_poly('cylinder.poly',2,'r'); 
    title(sprintf('TE: %g Hz',stMT.frequencies(i))); 


    subplot(2,nf,nf*(i-1)+2);
    sub_pcolorField(y,z,Hx(:,:,i),'log_{10}|Hx| (A/m)')
    sub_plotPoynting(iskip,y,z,Sy_TM,Sz_TM,'k') 
    sub_polarizationEllipse(iskip,y,z,Ey(:,:,1),Ez(:,:,1),'w',1) 
    m2d_plot_poly('cylinder.poly',2,'r'); 
    title(sprintf('TM: %g Hz',stMT.frequencies(i))); 
    
end
 
%--------------------------------------------------------------------------;
function sub_pcolorField(y,z,F,cstr)

hp = pcolor(y,z,log10(abs(F(:,:,1))));
set(hp,'tag','file');
axis ij; 
hold on;
shading flat;
xlabel('y(m)');
ylabel('z(m)');
box on;
c = colorbar;
c.Label.String = cstr;
set(gca,'layer','top','tickdir','out','fontsize',12);
axis equal tight;
axis manual


end
%--------------------------------------------------------------------------;
function sub_plotPoynting(iskip,y,z,Sy,Sz,color)

% option for streamlines:
% [Y,Z]=meshgrid(y,z);
% s1 = streamline(Y,Z,Sy,Sz,Y(1,:),Z(1,:));
% set(s1,'Color',color);

% normalize to unit length:
Sm = sqrt(Sy.^2 + Sz.^2);
Sy = Sy./Sm;
Sz = Sz./Sm;

% plot
scale = 0.5;
q1 = quiver(y(1:iskip:end,1:iskip:end),z(1:iskip:end,1:iskip:end),...
            Sy(1:iskip:end,1:iskip:end,1),Sz(1:iskip:end,1:iskip:end,1),...
            scale,color);

end

%--------------------------------------------------------------------------;
function sub_polarizationEllipse(iskip,y,z,Fy,Fz,color,linewidth) 

% Amplitudes and phase of e:
ax = abs(Fy);
az = abs(Fz);
px = atan2(imag(Fy),real(Fy));
pz = atan2(imag(Fz),real(Fz));

% Vertical polarization ellipse parameters:  
dp = pz-px; 
alpha = 1/2*atan2(2*ax.*az.*cos(dp),(ax.^2-az.^2));
pmax = abs(az.*exp(1i*dp).*sin(alpha) + ax.*cos(alpha));
pmin = abs(az.*exp(1i*dp).*cos(alpha) - ax.*sin(alpha));

% Compute ellipses to draw:
wt = linspace(0,2*pi,51);

for it = 1:length(wt)
    
    sx = ax.*cos(wt(it) - px); 
    sz = az.*cos(wt(it) - pz);

    sxa = sx./pmax;
    sza = sz./pmax;
    
    ell_ex(:,:,it) = sxa(1:iskip:end,1:iskip:end);
    ell_ez(:,:,it) = sza(1:iskip:end,1:iskip:end);
    
end % loop over time

dy = max(diff(y(1:iskip:end)));

[Y,Z]=meshgrid(y,z);

Y = Y(1:iskip:end,1:iskip:end);
Z = Z(1:iskip:end,1:iskip:end);

%plot ellipses:
for i = 1:size(ell_ex,1)
    for j=1:size(ell_ex,2)
        e1 = squeeze(ell_ex(i,j,:))*dy/2;
        e2 = squeeze(ell_ez(i,j,:))*dy/2;
        plot( Y(i,j)+real(e1),Z(i,j)+real(e2),color,'linewidth',linewidth,'tag','ellipse')
        hold on;
      end
end
 
end
%--------------------------------------------------------------------------;
function [F,iRx,iFq] = getComplexField(DATA,component)

switch lower(component)
    case 'ex'
        lReal = DATA(:,1) == 151;
        lImag = DATA(:,1) == 152;
    case 'ey'
        lReal = DATA(:,1) == 153;
        lImag = DATA(:,1) == 154;        
    case 'ez'
        lReal = DATA(:,1) == 155;
        lImag = DATA(:,1) == 156;        
    case 'hx'
        lReal = DATA(:,1) == 161;
        lImag = DATA(:,1) == 162;        
    case 'hy'
        lReal = DATA(:,1) == 163;
        lImag = DATA(:,1) == 164;
    case 'hz'
        lReal = DATA(:,1) == 165;
        lImag = DATA(:,1) == 166;        
end
if length(find(lReal)) ~=  length(find(lImag))
    beep;
    h = errordlg('Error, real and imaginary components have different number of elements. Is data file setup correctly?','Error forming complex data', 'modal');
    waitfor(h);
    return
end

F = DATA(lReal,7) + 1i*DATA(lImag,7);

iFqReal = DATA(lReal,2);
iRxReal = DATA(lReal,4);
iFqImag = DATA(lImag,2);
iRxImag = DATA(lImag,4);

if iFqReal ~= iFqImag
    beep;
    h = errordlg('Error, real and imaginary components have different frequency layout. Is data file setup correctly?','Error forming complex data', 'modal');
    waitfor(h);
    return    
end

if iRxReal ~= iRxImag
    beep;
    h = errordlg('Error, real and imaginary components have different receiver layout. Is data file setup correctly?','Error forming complex data', 'modal');
    waitfor(h);
    return 
end

iRx = iRxReal;
iFq = iFqReal;

end





 