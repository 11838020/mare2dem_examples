 
sFile = 'slab_fields.emdata';   % output file name
topo  = [];

clear st

st.stMT.frequencies  = logspace(-2,2,5);

st.stMT.rx_y         = [-100:5:-10 -9.5:.5:9.5 10:5:100]*1e3;
st.stMT.rx_z         = 0.01 + zeros(size(st.stMT.rx_y));
  
st.stMT.lMTFields    = true;    % create both TE and TM mode data
 

% make data:
m2d_makeDataFile(sFile,topo,'forward',st)

% plot .resistivity file with MT receivers on top to confirm receivers are
% located at the correct positions and depth:
plotMARE2DEM('newest'); % 'newest' plots the most recent .resistivity file
 
 