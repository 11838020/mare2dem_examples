This example shows the raw MT fields and responses for the segmented
slab model considered in the MARE2DMT paper (MARE2DEM's progenitor):

Key, K., & Weiss, C. (2006). Adaptive finite-element modeling using
unstructured grids: The 2D magnetotelluric example. Geophysics, 71(6),
G291–G299. https://doi.org/10.1190/1.2348091

This segmented slab model originated in these two papers:

Weaver, J., B. LeQuang, and G. Fischer, 1985, A comparison of analytic
and numerical results for a two-dimensional control model in
electromagnetic induction, Part I, B-polarization calculations:
Geophysical Journal of the Royal Astronomical Society, 82, 263–277. 

——–,1986, A comparison of analytical and numerical results for a 2-d control
model in electromagnetic induction, Part II, E-polarization
calculations: Geophysical Journal of the Royal Astronomical Society, 87,
917–948.

makeForwardData.m  makes the data file for forward modeling with
MARE2DEM.  

plotFields.m can be used to plot the results.
