% 

%
% The forward response file to read in:
%
fileName = 'slab.0.resp';

%
% Read the response file in:
% 
st = m2d_readEMData2DFile(fileName);

% peel off substructures:
stMT = st.stMT;
DATA = st.DATA;
 
% Get complex fields:
[Ex,iRx,iFq] = getComplexField(DATA,'ex');
[Ey,iRx,iFq] = getComplexField(DATA,'ey');
[Ez,iRx,iFq] = getComplexField(DATA,'ez');
[Hx,iRx,iFq] = getComplexField(DATA,'hx');
[Hy,iRx,iFq] = getComplexField(DATA,'hy');
[Hz,iRx,iFq] = getComplexField(DATA,'hz');

% Convert to arrays that are nRx x nFreq:
Ex = reshape(Ex,length(stMT.receivers(:,1)),length(stMT.frequencies));
Ey = reshape(Ey,length(stMT.receivers(:,1)),length(stMT.frequencies));
Ez = reshape(Ez,length(stMT.receivers(:,1)),length(stMT.frequencies));
Hx = reshape(Hx,length(stMT.receivers(:,1)),length(stMT.frequencies));
Hy = reshape(Hy,length(stMT.receivers(:,1)),length(stMT.frequencies));
Hz = reshape(Hz,length(stMT.receivers(:,1)),length(stMT.frequencies));

yRx = stMT.receivers(:,2)/1d3;

% Plot raw fields:
figure;
subplot(3,2,1);
semilogy(yRx,abs(Ex)); title('|Ex|'); xlabel('position (km)');
subplot(3,2,3);
semilogy(yRx,abs(Ey)); title('|Ey|'); xlabel('position (km)');
subplot(3,2,5);
semilogy(yRx,abs(Ez)); title('|Ez|'); xlabel('position (km)');
subplot(3,2,2);
semilogy(yRx,abs(Hx)); title('|Hx|'); xlabel('position (km)');
subplot(3,2,4);
semilogy(yRx,abs(Hy)); title('|Hy|'); xlabel('position (km)');
subplot(3,2,6);
semilogy(yRx,abs(Hz)); title('|Hz|'); xlabel('position (km)');

legend(num2str(stMT.frequencies))

set(findobj(gcf,'type','axes'),'fontsize',12)
set(findobj(gcf,'type','line'),'linewidth',1)
grid on;


% impedance/apparent resisivity check:
Z_te = Ex./Hy;
Z_tm = Ey./Hx;

mu = 4*pi*1d-7;
 
f = repmat(stMT.frequencies,1,length(yRx))';
 

rho_te = 1./(f*mu).*abs(Z_te).^2;
rho_tm = 1./(f*mu).*abs(Z_tm).^2;

figure;
subplot(2,2,1);
semilogy(yRx,abs(rho_te)); title('TE Apparent Resistivity'); xlabel('position (km)');
subplot(2,2,2);
semilogy(yRx,abs(rho_tm)); title('TM Apparent Resistivity'); xlabel('position (km)');
subplot(2,2,3);
plot(yRx,180/pi*angle(Z_te)); title('TE Phase'); xlabel('position (km)');
subplot(2,2,4);
plot(yRx,180/pi*angle(Z_tm)+180); title('TM Phase'); xlabel('position (km)');
legend(num2str(stMT.frequencies))

set(findobj(gcf,'type','axes'),'fontsize',12)
set(findobj(gcf,'type','line'),'linewidth',1)
grid on;

%--------------------------------------------------------------------------;
function [F,iRx,iFq] = getComplexField(DATA,component)

switch lower(component)
    case 'ex'
        lReal = DATA(:,1) == 151;
        lImag = DATA(:,1) == 152;
    case 'ey'
        lReal = DATA(:,1) == 153;
        lImag = DATA(:,1) == 154;        
    case 'ez'
        lReal = DATA(:,1) == 155;
        lImag = DATA(:,1) == 156;        
    case 'hx'
        lReal = DATA(:,1) == 161;
        lImag = DATA(:,1) == 162;        
    case 'hy'
        lReal = DATA(:,1) == 163;
        lImag = DATA(:,1) == 164;
    case 'hz'
        lReal = DATA(:,1) == 165;
        lImag = DATA(:,1) == 166;        
end
if length(find(lReal)) ~=  length(find(lImag))
    beep;
    h = errordlg('Error, real and imaginary components have different number of elements. Is data file setup correctly?','Error forming complex data', 'modal');
    waitfor(h);
    return
end

F = DATA(lReal,7) + 1i*DATA(lImag,7);

iFqReal = DATA(lReal,2);
iRxReal = DATA(lReal,4);
iFqImag = DATA(lImag,2);
iRxImag = DATA(lImag,4);

if iFqReal ~= iFqImag
    beep;
    h = errordlg('Error, real and imaginary components have different frequency layout. Is data file setup correctly?','Error forming complex data', 'modal');
    waitfor(h);
    return    
end

if iRxReal ~= iRxImag
    beep;
    h = errordlg('Error, real and imaginary components have different receiver layout. Is data file setup correctly?','Error forming complex data', 'modal');
    waitfor(h);
    return 
end

iRx = iRxReal;
iFq = iFqReal;

end





 